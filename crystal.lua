local crystal = {}
function crystal.object(node, position, scale, rotate, properties)
        properties = properties or {}
        local inner = am.translate(position) ^ am.scale(scale) ^ am.rotate(rotate) ^ node:tag("node")
        local wrap = am.wrap(inner)
        function wrap:set_properties(newvalue)
                properties = newvalue
        end
        function wrap:get_properties()
                return properties
        end
        function wrap:set_position(newvalue)
                position = newvalue
                inner("translate").position2d = position
        end
        function wrap:get_position()
                return position
        end
        function wrap:set_scale(newvalue)
                scale = newvalue
                inner("scale").scale2d = scale
        end
        function wrap:get_scale()
                return scale
        end
        function wrap:set_rotation(newvalue)
                rotate = newvalue
                inner("rotate").angle = rotate
        end
        function wrap:get_rotation()
                return rotate
        end
        function wrap:get_node()
                return inner("node")
        end
        function wrap:get_crystal_object()
                return true
        end
        wrap.rect = crystal.object_rect
        wrap.mouse_touches = crystal.mouse_touches
        return wrap
end

function crystal.sprite(sprite, position, color, scale, rotate, properties)
        local newnode = crystal.object(am.sprite(sprite), position, scale, rotate, properties)
        function newnode:set_sprite(newvalue)
                sprite = newvalue
                newnode.node.source = sprite
        end
        function newnode:get_sprite()
                return sprite
        end
        function newnode:set_color(newvalue)
                color = newvalue
                newnode.node.color = color
        end
        function newnode:get_color()
                return color
        end
        function newnode:get_color()
                return color
        end
        function newnode:get_width()
                return newnode.node.width * newnode.scale.x
        end
        function newnode:get_height()
                return newnode.node.height * newnode.scale.y
        end
        newnode.aabb_collision = crystal.aabb_collision
        return newnode
end

function crystal.text(text, position, color, scale, rotate, properties)
        local newnode = crystal.object(am.text(text, color), position, scale, rotate, properties)
        function newnode:set_text(newvalue)
                text = newvalue
                newnode.node.text = text
        end
        function newnode:get_text()
                return text
        end
        function newnode:get_width()
                return newnode.node.width * scale.x
        end
        function newnode:get_height()
                return newnode.node.height * scale.y
        end
        function newnode:set_color(newvalue)
                color = newvalue
                newnode.node.color = color
        end
        function newnode:get_color()
                return color
        end
        newnode.aabb_collision = crystal.aabb_collision
        return newnode
end

function crystal.get_object_by_property(node, name, value)
        if node and node.child_pairs then
                for index, child in node:child_pairs() do
                        if child.properties then
                                if child.properties[name] == value then
                                        return child
                                end
                        end
                end
        end
end

function crystal.get_all_objects_by_property(node, name, value)
        local matchednodes = {}
        for index, child in node:child_pairs() do
                if child.properties then
                        if child.properties[name] == value then
                                table.insert(matchednodes, child)
                        end
                end
        end
        return matchednodes
end

function crystal.keys_down(window, ...)
        for index, value in ipairs(arg) do
                if window:key_down(value) then
                        return true
                end
        end
        return false
end

function crystal.keys_released(window, ...)
        for index, value in ipairs(arg) do
                if window:key_released(value) then
                        return true
                end
        end
        return false
end

function crystal.keys_pressed(window, ...)
        for index, value in ipairs(arg) do
                if window:key_pressed(value) then
                        return true
                end
        end
        return false
end

function crystal.aabb_collision(node1, node2)
        if not node1.crystal_object or not node2.crystal_object then
                return false
        end
        return not (node2.position.x-node2.width/2 > node1.position.x+node1.width/2 or
                        node2.position.x+node2.width/2 < node1.position.x-node1.width/2 or
                        node2.position.y+node2.height/2 < node1.position.y-node1.height/2 or
                        node2.position.y-node2.height/2 > node1.position.y+node1.height/2)
end

function crystal.cartesian_to_screen(x, y, screenwidth, screenheight)
        local screenx = x + screenwidth / 2;
        local screeny = screenheight / 2 - y;
        return vec2(screenx, screeny)
end

function crystal.screen_to_cartesian(x, y, screenwidth, screenheight)
        local cartesianx = x - screenwidth / 2;
        local cartesiany = screenheight / 2 - y;
        return vec2(cartesianx, cartesiany)
end

function crystal.object_rect(object)
        return vec2(object.x - object.width/2, object.y - object.height/2), vec2(object.width, object.height)
end

function crystal.full_rect_to_half(x, y, width, height)
        return vec2(x - width/2, y + height/2), vec2(width, height)
end

function crystal.half_rect_to_full(x, y, width, height)
        return vec2(x + width/2, y + height/2), vec2(width, height)
end

function crystal.mouse_touches(node, window)
        local mousepos = window:mouse_position()
        if not node.crystal_object then
                return false
        end
        return mousepos.x >= node.position.x - node.width/2 and
                                        mousepos.x <= node.position.x + node.width/2 and
                                        mousepos.y >= node.position.y - node.height/2 and
                                        mousepos.y <= node.position.y + node.height/2
end

return crystal
