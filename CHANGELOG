0.01: First release, basic "all-in-one" objects (translation, scale, rotation)
0.02: Update not put up on GitLab (full changelog not available)
    * Very simple timers through crystal.timers
        - Create a timer holder with crystal.timers.create()
        - Add timers to a holder with crystal.timers.add()
        - Process and test for effect in one function: crystal.timers.process()
        - For more advanced things, crystal.timers.getmax(), crystal.timers.finished() and crystal.timers.reset()
        - All the above functions except crystal.timers.create() can also be accessed through the timer holder (holder:add() etc.)
0.03: Animation support
    * Element-based animation instead of stylesheet animation
        - Manipulate "elements" with a frame table - define their position, scale etc.
        - Smaller files - no need to store every single frame, just every single separate object
        - Accessible through crystal.anim
        - Create an animation object with crystal.anim.create()
        - Add/remove elements through crystal.anim.addelement() and crystal.anim.removeelement()
        - Add (soon remove) frames through crystal.anim.addframe()
        - Set and get the current frame through crystal.anim.setframe() and crystal.anim.getframe()
        - Proceed the animation (with automatic looping at the end) through crystal.anim.process()
        - Construct a sprite group of all the elements with crystal.anim.construct()
        - Create an auto-updating node for an anim object with crystal.anim.object()
0.031 (incorrectly pushed as 0.035): Improved animation objects
    * Animation objects made by crystal.anim.construct() and crystal.anim.object() are now Crystal objects (crystal.object())
        - This lets the code change the position, scale and rotation of the whole animated object as a whole
0.032: Further improved animation
    * Framesets allow you to separate your animations
        - Manage framesets through the crystal.anim.*frameset() set of functions
        - Use crystal.anim.addframeset() and anim:removeframeset() to respectively add or remove a frameset
        - Set or get the current frameset using crystal.anim.setframeset() and anim:removeframeset()
    * The crystal.anim.addframe now has an argument that allows you to specify how much the frame should be added
        - 1 is the default
        - For example, entering 10 would put the frame in the current frameset 10 times
0.033: Improved removing/adding of frames in animations
        * Each frame now gets an id value
                - This makes IDs returned by crystal.anim.addframe() static, meaning you don't have to keep track of how the frame table has changed
                - Repeated frames added using crystal.anim.addframe() share the same ID, meaning that providing the ID to crystal.anim.removeframe() will remove all of them
        * The function crystal.anim.addframe() now has a new (fourth) argument
                - If true, repeated frames will have different IDs
                - Has no effect if the third argument (repeatframes) is 1 or nil/false
0.034: Animation elements can now have transformations
        * The function crystal.anim.addelement() now has three new arguments: translate, rotate, scale
                - These transformations are superimposed on frames
                - For example, if you have an element with scale vec2(2,2) and a frame has a scale vec2(2, 2), the sprite will be scaled to vec2(4,4)
0.035 (the real one): Sane animation objects
        * Animation object nodes are now sanely coded
                - Before, the node would see if the frame or the frame set has changed and then it would replace itself with a new node (made through :object())
                - Now, the node is a group node that replaces contents of itself, not actually itself
                - This makes it so that you can actually attach things like actions or tags to the node and they won't disappear next frame
0.35: Versioning scheme change
        * To avoid confusion, the versioning scheme was changed (0.035 to 0.35)
                - There's no actual features changed from 0.035
0.4: Modularity
        * Amulet functions have been moved into a new namespace
                - Access them with crystal.amulet
        * Crystal is now separated in three parts: amulet, anim and timers
                - By default, no modules are loaded
                - If you try to use a function from a module that is not loaded, that module will be loaded and your program will continue
        * Load chosen modules
                - Use the load() function to reload the namespace with new modules, e.g.:
                        crystal = require("crystal").load("amulet", "anim") -- Load Crystal with crystal.amulet and crystal.anim
                        crystal.load("timers") -- Reload Crystal with only crystal.timers
                - You can also call the Crystal module directly and it will act as if you called the load() function:
                        crystal = require("crystal")("amulet", "anim")
                        crystal("timers")
        * Basic dependency testing
                - Dependencies aren't resolved, only tested for
                - There is no special support for dependency testing
                - Currently only the crystal.anim module does this for crystal.anim.construct() and crystal.anim.object()
0.41: Modularity improvements
        * New function crystal.shallowcopy as a replacement to Amulet's table.shallow_copy
                - Will eventually be moved to a separate, "general" module
        * Module crystal.anim no longer wrongly depends on Amulet
                - Previously, crystal.anim was unusable outside of Amulet because of the usage of table.shallow_copy and vectors
                - You can still use Amulet vectors when you're running the game with Amulet
                - If you don't use Amulet, use tables as vectors; e.g.:
                        anim:addframe({{element = 1, translate = vec2(15, 15)}})
                        -- these lines are equivalent
                        anim:addframe({{element = 1, translate = {15, 15}}})
0.42: Remove the Amulet dependency from crystal.timers
        * If Amulet is not detected, the crystal.timers.process() function will accept a delta time argument
0.5: Crystal does just one job now
        * I went a bit overboard with the module, basically reimplementing a submodule system and reinventing the wheel
        * Crystal is now just the amulet.lua module, the timers module is abandoned and the animation system is now a separate project
                - The separate project is called "elani" and will soon get its own repository
