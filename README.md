# Crystal
Crystal is a Lua module that contains a bunch of functions I found useful for making games, like timers or animations.  
# Load the module
```
local crystal = require("crystal")
```
# Planned features
* Better function for iterating animation frame objects (for non-Amulet users)
* More error checking
* "General" module with general-purpose functions

# Examples
Bullshit Bullets (available at https://gitlab.com/zatherz/bullshitbullets) is a simple game written in Amulet with Crystal.

# Documentation
The project is not finished yet. Documentation will come once 1.0.0 is released.
